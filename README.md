# withProfile

A repo for wrapping components with a withProfile HOC to ensure loading of users.
A spinner is optionally shown while the profile is being fetched.

### Index
[Quick guide](#quick-guide)  
[HOC Wrapping function](#wrapping-function)  
[Utility functions](#utility-functions)  
[Saving profile updates](#saving-profile-updates)  
[Setting config and default](#setting-config-and-default)  



# Quick guide
Add `"with-profile": "git+https://bitbucket.org/allbin/with-profile.git#v`**`x.y.z`** to package.json. Where **x.y.z** is the version tagged.

Use `import withProfile from 'with-profile'` in any file and wrap desired component in it to ensure profile is loaded before rendering.

### Example MyComponent.jsx component:
```
import React from 'react';
import withProfile from 'with-profile';

class MyComponent extends React.Component {
    ...
}

export default withProfile(MyComponent);
```

### Example inline component:
```
import React from 'react';
import withProfile from 'with-profile';
import SomeComponent from './components/SomeComponent';

let SomeComponentWithProfile = withProfile(SomeComponent);

export default class MyComp extends React.Component {
    ...

    render() {
        return (
            <div>
                <SomeComponentWithProfile />
            </div>
        );
    }
}

```

### Example with configuration and store integration
```
import React from 'react';
import withProfile from 'with-profile';
import withStore from 'with-store';
import SomeComponent from './components/SomeComponent';
import ChildComponent from './components/ChildComponent';

withProfile.config({
    url: "http://url.to.profile/:user_id",
    store: withStore
});

let SomeComponentWithProfile = withProfile(SomeComponent);

export default class MyComp extends React.Component {
    ...

    render() {
        return (
            <SomeComponentWithProfile>
                <ChildComponent />
            </SomeComponentWithProfile>
        );
    }
}

```
**NOTE:** SomeComponent and ChildComponent will only show after fetch is complete.

# Known issues
- The non-optistimic saves (`withProfile.saveProfile()`) do currently not wait for each other, if multiple saves are made before the previous has finished they may overwrite whatever changes the previous call made. A queue should be added.



# Wrapping function
The HOC wrapping function attaches the `profile` prop to the component being wrapped.

Example: `withProfile(YourComponent)` will give access to `this.props.profile` inside *YourComponent*.

### Optional parameters
#### SpinnerComponent
*withProfile(YourComponent, **`SpinnerComponent`**)*.  
Optionally a second component can be supplied which will be shown while the profile is being fetched. The spinner component, if supplied, also gets all props assigned to the wrapped component.



# Utility functions
`withProfile.addStore(<store>)` - withProfile will call the supplied stores addState function with an object containing a reducer and actions for use with a redux store; *store.addState({ reducer: fn, actions: {...} })*.  
The state will automatically be populated with the profile either when it is finished fetching or immediately if the profile is already fetched. Can be called multiple times.

`withProfile.configure(<config object>, <default profile>)` - See [Setting config and default](#setting-config-and-default).

`withProfile.doAfterFirstFetch(<callback>)` - The callback will be called with the profile as argument after the first time the profile has been fetched. Can be called multiple times, callbacks will be called in the order they were added.

`withProfile.fetch(<boolean, optional to force reload>)` - Returns a promise that resolves to the profile values, any default values merged with any fetched values.  
If the fetch has not been initiated it will be by this call and it will resolve once finished. First argument, *force reload* as true will force a refetch from server.

`withProfile.getProfile()` - Returns the profile values currently in withProfile. Note that this function returns whatever is currently in profile, even if fetch has not finished. Sync version of *fetch()*.

`withProfile.saveProfile(<updated_profile>, <replace>)`  - See [Saving profile updates](#saving-profile-updates).  
`withProfile.saveProfileOptistic(<updated_profile>, <replace>)` - See [Saving profile updates](#saving-profile-updates).

`withProfile.setFetchedCallback(<callback function>)` - If set the callback will trigger when fetching profile from server has completed. The callbacks only parameter will be the final config object. Note that this function will trigger every time a fetch is completed, such as when `withProfile.fetch(true)` is run to force a refetch and when `withProfile.saveProfile(updated_profile)` is used.



# Saving profile updates
`withProfile.saveProfile(<updated_profile>, <replace>)`  
Used to save an updated profile to the server, returns a Promise which resolves to the new profile when the server request has finished.  
***updated_profile*** -  object, required
> Object to save to the server.

***replace*** - boolean, default false
>When true *updated_profile* will replace existing profile. Otherwise existing profile will be extended with *updated_profile*.  

`withProfile.saveProfileOptimistic(<updated_profile>, <replace>)`  
Will optimistically trigger updates of components and states. Returns a Promise which resolves to the profile as the server believes it is when request is complete.  
***NOTE:** This is only advisable if the profile write to server is not critical for application operation.*  
***NOTE:** You may catch request errors on the returned promise.*  
***NOTE:** An opimistic save issued between the issuing and completion of a non-optimistic save will first overwrite the optimistic save with the result of the non-optimistics request and then when the opimistic server request returns overwrite (and delete) the changes of the non-optimistic.*

***NOTE: BE AWARE that if these functions are called before first fetch has completed it will act as a partial replace. Since it will send an updated profile to the server that doesn't contain whatever the server currently has!** Use `doAfterFirstFetch` to register a callback to know when it is safe to proceed with profile saving.*



# Setting config and default
Use `withProfile.config(<config_object>, <default_profile>)` to configure and set default profile. Returns nothing.

**config_object** - object, required
```
    {
        url: <required, string, url to the profile>,
        token: <required, function that returns an authentication token string>
        store: <optional>
    }
``` 
>Supplying a **store** in config is equivalent to `withProfile.addStore(<store>)`.

**default_profile** - object, optional
> Default profile can be any object with any properties but will be overwritten by fetched profile.



