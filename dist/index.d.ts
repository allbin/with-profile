import * as React from 'react';
import * as state from './state';
export interface WithProfileState {
    loading: boolean;
}
export interface ProfileStore {
    [key: string]: any;
    addState: (prefix: string, state_definition: state.StateDefinition) => {
        actions: state.WrappedActions;
    };
}
export interface ConfigObject {
    token: () => string;
    url: string;
    store?: ProfileStore;
}
export interface Profile {
    [key: string]: any;
}
export declare function WithProfileHOC(WrappedComponent: typeof React.Component, SpinnerComponent?: typeof React.Component): typeof React.Component;
export declare namespace WithProfileHOC {
    function config(cfg: ConfigObject, new_default_profile?: Profile): void;
    function addStore(store: ProfileStore): void;
    function doAfterFirstFetch(cb: (Profile: any) => void): void;
    function fetch(force_reload?: boolean): Promise<Profile>;
    function getProfile(): Profile;
    function saveProfile(data: any, replace?: boolean): Promise<Profile>;
    function saveProfileOptimistic(data: any, replace?: boolean): Promise<Profile>;
    function setFetchedCallback(cb: any): void;
}
export default WithProfileHOC;
//# sourceMappingURL=index.d.ts.map