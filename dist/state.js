"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SET_PROFILE_STATE = "SET_PROFILE_STATE";
var default_state = {
    profile_initialized: false
};
var reducer = function (state, action) {
    if (state === void 0) { state = default_state; }
    switch (action.type) {
        case SET_PROFILE_STATE: {
            return Object.assign({}, action.payload);
        }
        default: {
            return state;
        }
    }
};
var actions = {
    set: function (state, config) {
        return {
            type: SET_PROFILE_STATE,
            payload: Object.assign({ profile_initialized: true }, config)
        };
    },
    update: function (state, new_profile_settings) {
        return {
            type: SET_PROFILE_STATE,
            payload: Object.assign({ profile_initialized: true }, state, new_profile_settings)
        };
    }
};
exports.state_definition = {
    reducer: reducer,
    actions: actions
};

//# sourceMappingURL=state.js.map
