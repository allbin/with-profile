"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var axios_1 = require("axios");
var state = require("./state");
var url = null;
var token = null;
var optimistic = true;
var stores = [];
var default_profile = {};
var fetched_profile = null;
var profile = {};
var fetched_cb = null;
var error_cb = null;
var fetching_error = null;
var fetching_status = 'not_initialized';
var get_profile_listeners = [];
var component_listeners = [];
var do_after_first_fetch = [];
function initiateFetch() {
    fetching_status = 'fetching';
    if (!url) {
        throw new Error("withProfile: url needs to be specified with withProfile.config({ url: ... }) command before fetch is triggered.");
    }
    if (!token) {
        throw new Error("withProfile: token function needs to be specified with withProfile.config({ token: () => { return <string>; }}).");
    }
    return axios_1.default({
        url: url,
        method: 'GET',
        headers: {
            'Authorization': "Bearer " + token()
        }
    }).then(function (res) {
        var first_fetch = fetched_profile === null; //This is the first time we fetched profile from server. Trigger callbacks.
        fetched_profile = res.data;
        profile = Object.assign({}, default_profile, fetched_profile);
        fetching_status = 'completed';
        stores.forEach(function (store) {
            store.actions.set(profile);
        });
        if (fetched_cb !== null) {
            fetched_cb(profile);
        }
        get_profile_listeners.forEach(function (listener) {
            listener();
        });
        setTimeout(function () {
            //Timeout to ensure execution order; component update last.
            component_listeners.forEach(function (listener) {
                listener();
            });
            if (first_fetch) {
                do_after_first_fetch.forEach(function (cb) {
                    cb(profile);
                });
                do_after_first_fetch = [];
            }
        }, 10);
    }).catch(function (err) {
        err.network_error = true;
        fetching_error = err;
        throw err;
    });
}
function fetchProfile(force_reload) {
    return new Promise(function (resolve, reject) {
        if (fetching_status === 'completed' && !force_reload) {
            resolve(profile);
            return;
        }
        if (fetching_status === 'failed') {
            reject(fetching_error);
            return;
        }
        get_profile_listeners.push(function () {
            if (fetching_status === 'completed') {
                resolve(profile);
                return;
            }
            reject(fetching_error);
            return;
        });
        if (fetching_status === 'not_initialized') {
            initiateFetch();
        }
    });
}
function pushProfileOpimistic(data, replace) {
    if (replace === void 0) { replace = false; }
    if (!replace) {
        data = Object.assign({}, default_profile, profile, data);
    }
    profile = data;
    stores.forEach(function (store) {
        store.actions.set(profile);
    });
    get_profile_listeners.forEach(function (listeners) {
        listeners();
    });
    setTimeout(function () {
        //Timeout to ensure execution order; component update last.
        component_listeners.forEach(function (listener) {
            listener();
        });
    }, 10);
    return axios_1.default({
        url: url,
        method: "PUT",
        data: data,
        headers: {
            'Authorization': "Bearer " + token()
        }
    }).then(function (res) {
        return res.data;
    });
}
function pushProfile(data, replace) {
    if (replace === void 0) { replace = false; }
    if (!replace) {
        data = Object.assign({}, default_profile, profile, data);
    }
    return axios_1.default({
        url: url,
        method: "PUT",
        data: data,
        headers: {
            'Authorization': "Bearer " + token()
        }
    }).then(function (profile_from_server) {
        profile = Object.assign({}, default_profile, profile_from_server.data);
        stores.forEach(function (store) {
            store.actions.set(profile);
        });
        get_profile_listeners.forEach(function (listeners) {
            listeners();
        });
        setTimeout(function () {
            //Timeout to ensure execution order; component update last.
            component_listeners.forEach(function (listener) {
                listener();
            });
        }, 10);
        return profile;
    });
}
function WithProfileHOC(WrappedComponent, SpinnerComponent) {
    var WithProfile = /** @class */ (function (_super) {
        __extends(WithProfile, _super);
        function WithProfile(props) {
            var _this = _super.call(this, props) || this;
            _this.state = { loading: true };
            return _this;
        }
        WithProfile.prototype.componentListener = function () {
            this.setState({ loading: false });
        };
        WithProfile.prototype.componentDidMount = function () {
            var _this = this;
            component_listeners.push(function () {
                _this.componentListener();
            });
            if (fetching_status === 'completed') {
                this.setState({ loading: false });
                return;
            }
            if (fetching_status === 'not_initialized') {
                initiateFetch();
            }
        };
        WithProfile.prototype.componentWillUnmount = function () {
            var listener_index = component_listeners.indexOf(this.componentListener);
            component_listeners.splice(listener_index, 1);
        };
        WithProfile.prototype.render = function () {
            if (this.state.loading) {
                if (SpinnerComponent) {
                    return React.createElement(SpinnerComponent, __assign({}, this.props));
                }
                return null;
            }
            return React.createElement(WrappedComponent, __assign({ profile: profile }, this.props));
        };
        return WithProfile;
    }(React.Component));
    return WithProfile;
}
exports.WithProfileHOC = WithProfileHOC;
(function (WithProfileHOC) {
    function config(cfg, new_default_profile) {
        if (new_default_profile === void 0) { new_default_profile = {}; }
        if (typeof default_profile !== 'object') {
            throw new Error('withProfile error: Argument default_profile is required to be an object.');
        }
        if (cfg.hasOwnProperty("url") === false || typeof cfg.url !== "string") {
            throw new Error("withProfile: config must contain property 'url' of type string.");
        }
        if (cfg.hasOwnProperty("token") === false || typeof cfg.token !== "function") {
            throw new Error("withProfile: config must contain property 'token' of type string.");
        }
        default_profile = new_default_profile;
        profile = Object.assign({}, default_profile, fetched_profile);
        url = cfg.url;
        token = cfg.token;
        if (cfg.store) {
            var i = stores.push(cfg.store.addState("profile", state.state_definition));
            if (fetching_status === "completed") {
                stores[i].actions.set(profile);
            }
        }
    }
    WithProfileHOC.config = config;
    function addStore(store) {
        var i = stores.push(store.addState("profile", state.state_definition));
        if (fetching_status === "completed") {
            stores[i].actions.set(profile);
        }
    }
    WithProfileHOC.addStore = addStore;
    function doAfterFirstFetch(cb) {
        if (fetched_profile !== null) {
            cb(profile);
        }
        else {
            do_after_first_fetch.push(cb);
        }
    }
    WithProfileHOC.doAfterFirstFetch = doAfterFirstFetch;
    function fetch(force_reload) {
        if (force_reload === void 0) { force_reload = false; }
        return fetchProfile(force_reload);
    }
    WithProfileHOC.fetch = fetch;
    function getProfile() {
        return profile;
    }
    WithProfileHOC.getProfile = getProfile;
    function saveProfile(data, replace) {
        if (replace === void 0) { replace = false; }
        return pushProfile(data, replace);
    }
    WithProfileHOC.saveProfile = saveProfile;
    function saveProfileOptimistic(data, replace) {
        if (replace === void 0) { replace = false; }
        return pushProfileOpimistic(data, replace);
    }
    WithProfileHOC.saveProfileOptimistic = saveProfileOptimistic;
    function setFetchedCallback(cb) {
        fetched_cb = cb;
    }
    WithProfileHOC.setFetchedCallback = setFetchedCallback;
})(WithProfileHOC = exports.WithProfileHOC || (exports.WithProfileHOC = {}));
exports.default = WithProfileHOC;

//# sourceMappingURL=index.js.map
