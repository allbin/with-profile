interface InitialState {
    [key: string]: any;
    profile_initialized: false;
}
export interface StoreActions {
    set: (state: any, config: any) => void;
    update: (state: any, config: any) => void;
}
export interface WrappedActions {
    set: (config: any) => void;
    update: (config: any) => void;
}
export interface StateDefinition {
    reducer: (state: InitialState, action: {
        type: string;
        payload: any;
    }) => void;
    actions: StoreActions;
}
export declare let state_definition: StateDefinition;
export {};
//# sourceMappingURL=state.d.ts.map