import * as React from 'react';
import axios from 'axios';
import * as state from './state';



export interface WithProfileState {
    loading: boolean;
}
export interface ProfileStore {
    [key: string]: any;
    addState: (prefix: string, state_definition: state.StateDefinition) => {
        actions: state.WrappedActions;
    };
}
export interface ConfigObject {
    token: () => string;
    url: string;
    store?: ProfileStore;
}

export interface Profile {
    [key: string]: any;
}

type Status = "not_initialized" | "fetching" | "completed" | "failed";

let url = null;
let token = null;
let optimistic = true;

let stores: {
    actions: state.WrappedActions;
}[] = [];

let default_profile: object = {};
let fetched_profile: object | null = null;
let profile: Profile = {};

let fetched_cb = null;
let error_cb = null;
let fetching_error = null;

let fetching_status: Status = 'not_initialized';
let get_profile_listeners = [];
let component_listeners = [];

let do_after_first_fetch: ((Profile) => void)[] = [];

function initiateFetch(): Promise<void> {
    fetching_status = 'fetching';

    if (!url) {
        throw new Error("withProfile: url needs to be specified with withProfile.config({ url: ... }) command before fetch is triggered.");
    }

    if (!token) {
        throw new Error("withProfile: token function needs to be specified with withProfile.config({ token: () => { return <string>; }}).");
    }

    return axios({
        url: url,
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${ token() }`
        }
    }).then((res) => {
        let first_fetch = fetched_profile === null; //This is the first time we fetched profile from server. Trigger callbacks.
        fetched_profile = res.data;
        profile = Object.assign({}, default_profile, fetched_profile);
        fetching_status = 'completed';
        stores.forEach((store) => {
            store.actions.set(profile);
        });
        if (fetched_cb !== null) {
            fetched_cb(profile);
        }
        get_profile_listeners.forEach((listener) => {
            listener();
        });
        setTimeout(() => {
            //Timeout to ensure execution order; component update last.
            component_listeners.forEach((listener) => {
                listener();
            });
            if (first_fetch) {
                do_after_first_fetch.forEach((cb) => {
                    cb(profile);
                });
                do_after_first_fetch = [];
            }
        }, 10);
    }).catch((err) => {
        err.network_error = true;
        fetching_error = err;
        throw err;
    });
}

function fetchProfile(force_reload): Promise<Profile> {
    return new Promise((resolve, reject) => {
        if (fetching_status === 'completed' && !force_reload) {
            resolve(profile);
            return;
        }
        if (fetching_status === 'failed') {
            reject(fetching_error);
            return;
        }
        get_profile_listeners.push(() => {
            if (fetching_status === 'completed') {
                resolve(profile);
                return;
            }
            reject(fetching_error);
            return;
        });
        if (fetching_status === 'not_initialized') {
            initiateFetch();
        }
    });
}


function pushProfileOpimistic(data: Profile, replace = false): Promise<Profile> {
    if (!replace) {
        data = Object.assign({}, default_profile, profile, data);
    }
    profile = data;
    stores.forEach((store) => {
        store.actions.set(profile);
    });
    get_profile_listeners.forEach((listeners) => {
        listeners();
    });
    setTimeout(() => {
        //Timeout to ensure execution order; component update last.
        component_listeners.forEach((listener) => {
            listener();
        });
    }, 10);

    return axios({
        url: url,
        method: "PUT",
        data: data,
        headers: {
            'Authorization': `Bearer ${ token() }`
        }
    }).then((res) => {
        return res.data;
    });
}

function pushProfile(data: Profile, replace = false): Promise<Profile> {
    if (!replace) {
        data = Object.assign({}, default_profile, profile, data);
    }
    return axios({
        url: url,
        method: "PUT",
        data: data,
        headers: {
            'Authorization': `Bearer ${ token() }`
        }
    }).then((profile_from_server: Profile) => {
        profile = Object.assign({}, default_profile, profile_from_server.data);
        stores.forEach((store) => {
            store.actions.set(profile);
        });
        get_profile_listeners.forEach((listeners) => {
            listeners();
        });
        setTimeout(() => {
            //Timeout to ensure execution order; component update last.
            component_listeners.forEach((listener) => {
                listener();
            });
        }, 10);
        return profile;
    });
}

export function WithProfileHOC (
    WrappedComponent: typeof React.Component,
    SpinnerComponent?: typeof React.Component
): typeof React.Component {
    class WithProfile extends React.Component<any, WithProfileState> {
        constructor(props) {
            super(props);

            this.state = { loading: true };
        }

        componentListener() {
            this.setState({ loading: false });
        }

        componentDidMount() {
            component_listeners.push(() => {
                this.componentListener();
            });
            if (fetching_status === 'completed') {
                this.setState({ loading: false });
                return;
            }
            if (fetching_status === 'not_initialized') {
                initiateFetch();
            }
        }

        componentWillUnmount() {
            let listener_index = component_listeners.indexOf(
                this.componentListener
            );
            component_listeners.splice(listener_index, 1);
        }

        render() {
            if (this.state.loading) {
                if (SpinnerComponent) {
                    return <SpinnerComponent {...this.props} />;
                }
                return null;
            }
            return <WrappedComponent profile={ profile } {...this.props} />;
        }
    }

    return WithProfile;
}

export namespace WithProfileHOC {
    export function config(cfg: ConfigObject, new_default_profile: Profile = {}) {
        if (typeof default_profile !== 'object') {
            throw new Error('withProfile error: Argument default_profile is required to be an object.');
        }
        if (cfg.hasOwnProperty("url") === false || typeof cfg.url !== "string") {
            throw new Error("withProfile: config must contain property 'url' of type string.");
        }
        if (cfg.hasOwnProperty("token") === false || typeof cfg.token !== "function") {
            throw new Error("withProfile: config must contain property 'token' of type string.");
        }
        default_profile = new_default_profile;
        profile = Object.assign({}, default_profile, fetched_profile);
        url = cfg.url;
        token = cfg.token;
        if (cfg.store) {
            let i = stores.push(cfg.store.addState("profile", state.state_definition));
            if (fetching_status === "completed") {
                stores[i].actions.set(profile);
            }
        }
    }
    export function addStore(store: ProfileStore): void {
        let i = stores.push(store.addState("profile", state.state_definition));
        if (fetching_status === "completed") {
            stores[i].actions.set(profile);
        }
    }
    export function doAfterFirstFetch(cb: (Profile) => void): void {
        if (fetched_profile !== null) {
            cb(profile);
        } else {
            do_after_first_fetch.push(cb);
        }
    }
    export function fetch(force_reload = false): Promise<Profile> {
        return fetchProfile(force_reload);
    }
    export function getProfile(): Profile {
        return profile;
    }
    export function saveProfile(data, replace = false): Promise<Profile> {
        return pushProfile(data, replace);
    }
    export function saveProfileOptimistic(data, replace = false): Promise<Profile> {
        return pushProfileOpimistic(data, replace);
    }
    export function setFetchedCallback(cb): void {
        fetched_cb = cb;
    }
}

export default WithProfileHOC;
